class UsersController < ApplicationController
  skip_before_action :authenticate_request, only: %i[login register]

  # POST /auth/register
  def register
    @user = User.create(user_params)
    if @user.save
      response = {message: 'User created successfully!.'}
      render json: response, status: :created
    else
      render json: @user.errors, status: :bad
    end
  end

  # POST /auth/login
  def login
    authenticate params[:email], params[:password]
  end

  # GET /users
  def list
    # if current_user.has_role? :admin
    if current_user.is_admin?
      @users = User.all
      render json: @users.to_json, status: 200
    else
      render json: {message: 'Not authorize'}, status: :unauthorized
    end
  end

  # GET /hello
  def hello
    render json: {
      message: 'You have passed authentication and authorization test!.'
    }
  end

  private

  def user_params
    params.permit(
      :username,
      :email,
      :password
    )
  end

  def authenticate(email, password)
    command = AuthenticateUser.call(email, password)

    if command.success?
      render json: {
        access_token: command.result,
        message: 'Login successful!.'
      }
    else
      render json: {error: command.errors}, status: :unauthorized
    end
  end
end
