Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # add register route
  post 'auth/register', to: 'users#register'
  post 'auth/login', to: 'users#login'
  get 'hello', to: 'users#hello'
  get 'users', to: 'users#list'
end
