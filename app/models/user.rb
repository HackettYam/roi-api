class User < ApplicationRecord
  rolify
  after_create :assign_default_role

  # Validations
  validates_presence_of :username, :email, :password_digest
  validates :username, uniqueness: true
  validates :email, uniqueness: true

  # encrypt password
  has_secure_password

  def assign_default_role
    self.add_role(:newuser) if self.roles.blank?
  end
end
